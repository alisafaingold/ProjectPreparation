package domain.Enums;

public enum TeamManagerPermissions {
    AddAsset,RemoveAsset,EditAsset,AddOwner,RemoveOwner,AddManager,RemoveManager,CloseTeam,OpenTeam,ReportFinance,EditPermissions
}
