package domain.Enums;

public enum UserStatus {
    LogIn, LogOut, NotActive;
}
