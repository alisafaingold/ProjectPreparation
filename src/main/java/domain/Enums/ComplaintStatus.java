package domain.Enums;

public enum ComplaintStatus {
    Closed, New, Archive;
}
