package domain.Enums;

public enum TeamState {
    active,
    notActive,
    permanentlyClosed
}
